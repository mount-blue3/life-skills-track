# Prevention of Sexual Harassment

### Answer1:

Sexual harassment refers to different types of behaviors that are considered inappropriate and offensive. Here are some examples of such behaviors:

- **Unwanted advances:** Making unwanted or unwelcome sexual advances towards someone, asking for sexual favors, or making inappropriate propositions.

- **Verbal harassment:** Using offensive or sexually explicit language, making offensive comments, or telling sexual jokes.

- **Physical harassment:** Touching someone inappropriately, groping, or making unwanted physical contact without their consent.

- **Sexual coercion:** Pressuring or forcing someone to engage in sexual activities against their will.

- **Sexual comments or gestures:** Making inappropriate comments about someone's body, appearance, or clothing, or using sexually suggestive gestures.

* **Stalking:** Continuously following, watching, or monitoring someone's activities without their permission.

* **Unwanted sexual attention:** Persistently engaging in unwelcome sexual behaviors or making someone feel uncomfortable through persistent advances.

* **Displaying explicit materials:** Showing or sharing explicit or pornographic materials in a place where it is not appropriate.

### Answer2:

- **Ensure my safety:** If I feel threatened or unsafe, remove myself from the situation and find a safe space.

- **Document the incidents:** Keep a record of any incidents, including dates, times, locations, and details of what occurred. This documentation can be valuable if I decide to report the harassment later.

* **Seek support:** Talk to someone I trust about the incidents, such as a friend, family member, or colleague. Their support can provide emotional reassurance and help me decide on the next steps.

* **Know my rights:** Familiarize myself with the policies and laws related to sexual harassment in my specific context, such as my workplace, educational institution, or local jurisdiction. Understand what constitutes sexual harassment and what actions can be taken to address it.

* **Seek professional guidance:** Consider consulting with a legal professional or seeking advice from organizations that specialize in handling cases of sexual harassment. They can provide me with guidance on my rights, legal options, and support services available.

### Answer3:

- Avoid using explicit or nude pictures as screensavers or photos.
- Respect co-workers' boundaries and refrain from persistently asking for something they have already declined.
- Refrain from making sexual or dirty jokes towards anyone.
- Obtain consent before touching or hugging someone, and respect their decision if they decline.
- Treat everyone equally and ensure no one is left isolated or excluded.
- Avoid exploiting authority to fulfill personal sexual desires.
- Refrain from using words with sexual connotations or double meanings in conversations.
- Avoid making inappropriate comments of a sexual nature.

### Answer4:

To handle cases of harassment, follow these steps:

- **Recognize the behavior:** Understand what counts as harassment and know the rules and laws related to it. Clearly identify the inappropriate actions or words involved.

- **Keep a record:** Write down the incidents, including dates, times, locations, and any witnesses. Note what happened and how it made you feel. This record can be useful if you decide to report the harassment later.

* **Stay safe:** Put your safety first. If you feel threatened, remove yourself from the situation or seek help from trusted colleagues, friends, or family.

* **Know the policies:** Learn your company's policies about harassment. Understand how to report it and what support services are available, like hotlines or counseling.

* **Report the harassment:** If you feel comfortable and safe, report the harassment to the right person or department in your organization. This might be your supervisor, human resources, or someone assigned to handle such issues. Follow the reporting process and provide all the relevant information and evidence you have.

* **Get support:** Talk to supportive people, like trusted colleagues, friends, or family. Sharing your experience with people who understand can help you cope with the emotions.

* **Keep it confidential:** If you want to stay anonymous, ask if there are ways to report the harassment without revealing your identity, or request that your identity be protected during the investigation.

### Answer5:

- **Respect boundaries:** Always be aware of personal space and boundaries. Obtain consent before initiating physical contact, such as hugging or touching.

- **Communicate respectfully:** Use polite and considerate language when speaking to others. Avoid offensive or derogatory remarks, including jokes with sexual or inappropriate content.

- **Be mindful of others:** Treat everyone with respect and fairness, regardless of their gender, race, religion, or other characteristics. Avoid making discriminatory or offensive comments.

- **Listen actively:** Pay attention to others when they speak and show genuine interest. Avoid interrupting or dominating conversations.

- **Maintain professionalism:** Follow the established rules and policies of your workplace or environment. Dress appropriately, use appropriate language, and refrain from engaging in behavior that may be deemed unprofessional.

- **Use technology responsibly:** Use digital devices and communication platforms in a manner that respects privacy and confidentiality. Avoid sharing inappropriate or offensive content.
