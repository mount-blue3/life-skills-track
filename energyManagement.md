# Energy Management

## Manage Energy not Time

## Question 1: What are the activities you do that make you relax - Calm quadrant?

#### Answer 1:

* Talking with my mom.

* Watching a good movie.

* Engage in painting.

* Have an interest of listening soft and melodious music.

* Take random walks, enjoy nature.

* Follow a routine of showering and getting a good night's sleep.

## Question 2: When do you find getting into the Stress quadrant?

#### Answer 2: 

* Feeling stressed when approaching deadlines with unfinished work.

* Experiencing anxiety when contemplating the uncertainty of the future.

* Doubting past decisions and engaging in self-doubt.

* Feeling unsure about personal goals and what to accomplish as an individual.

## Question 3: How do you understand if you are in the Excitement quadrant?

#### Answer 3:

* Getting completely focused on the present moment and not caring about anything else.

* Feeling proud and accomplished when achieving a goal I thought was impossible.

* Feeling satisfied after finishing a painting, or acquiring a new skill.

* Experiencing happiness when cooking and creating something delicious.

* Feeling enthusiastic about learning new things, especially in areas where I can grow and improve.

## Sleep is your superpower

## Question 4:Paraphrase the Sleep is your Superpower video in detail.

#### Answer 4:

* Insufficient sleep negatively affects reproductive health in both males and females.

* Adequate sleep is crucial for learning and memory functions in the brain.

* Sleep is essential before and after learning to consolidate memories.

* Lack of sleep disrupts memory consolidation and hinders the acquisition of new memories.

* Sleep deprivation reduces the brain's ability to form new memories by 40%.

* Sleep deprivation impairs the brain's processing of new experiences and memory formation.

* Quality sleep is necessary to restore and enhance memory and learning capabilities.

* Sleep spindles during deep sleep aid in transferring memories from short-term to long-term storage.

* Disrupted deep sleep contributes to cognitive decline in aging and Alzheimer's disease.

* Understanding sleep physiology can potentially improve memory, learning, and prevent cognitive decline.

## Question 5: What are some ideas that you can implement to sleep better?

#### Answer 5:

* Stick to a consistent sleep schedule.

* Establish a calming bedtime routine.

* Create a sleep-friendly environment (dark, quiet, and comfortable).

* Limit screen time before bed.

* Engage in regular exercise.

* Invest in a comfortable sleep environment (mattress, pillows, bedding).

* Manage stress and anxiety before bed.

## Question 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

#### Answer 6: 

* Physical activity benefits the brain by protecting against conditions like depression, Alzheimer's, and dementia.

* The prefrontal cortex and hippocampus play key roles in decision-making, attention, personality, and memory formation.

* A neuroscientist studying brain cells in the hippocampus realized the need for changes due to a sedentary lifestyle.

* Incorporating regular exercise improved the speaker's mood, energy levels, and resulted in weight loss.

* Exercise also enhanced the speaker's focus and long-term memory.

* Scientific evidence supports the positive effects of physical activity on mood, energy, memory, and attention.

## Question 7: What are some steps you can take to exercise more?

#### Answer 7: 

* Set specific goals for exercise.

* Start with small steps and gradually increase intensity.

* Find enjoyable activities.

* Schedule exercise into your day.

* Exercise with a buddy or join a group.

* Establish a regular exercise routine.

* Mix up your workouts for variety.

* Set realistic expectations and be patient.

* Track your progress.

* Stay motivated through rewards and support.





