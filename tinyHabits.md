# Tiny Habits

## Question 1: Your takeaways from the video (Minimum 5 points)

#### Answer 1:

* BJ Fogg, a Stanford University professor, focuses on behavior change and its design.

* He introduces "tiny habits" as a technique for making small behavior changes.

* The audience participates in a "tiny habit" exercise of flossing one tooth and celebrating the accomplishment.

* Fogg discusses different ways to celebrate behavior change victories, like positive affirmations or dancing.

* He shares his personal experience of incorporating exercise into his routine by doing push-ups after using the restroom.

* Fogg explains that there are 15 ways behaviors can change, with changing the environment and relying on motivation being key for long-term change.

* Fogg emphasizes that behavior change is not as complicated as people often perceive it to be.

* He encourages the audience to focus on small, incremental changes rather than overwhelming themselves with big goals.

* Fogg explains that the key to long-term behavior change is to make it easy and convenient for oneself.

* He suggests that people design their environment in a way that supports the desired behavior change.

## Question 2: Your takeaways from the video in as much detail as possible

#### Answer 2:

* According to the "Tiny Habits" method by BJ Fogg, human behavior can be explained by the formula B = MAP (Behavior = Motivation + Ability + Prompt).

* To develop a new habit, it should be made as small as possible, achievable in 30 seconds or less.

* A prompt, either external or internal, is crucial to remind and prompt the desired behavior.

* The habit is nurtured and grown over time, with ongoing effort and attention.

* By using this method, one can create lasting behavior changes by focusing on motivation, ability, and consistent prompts, rather than relying solely on motivation.

* Fogg emphasizes that it is easier to change behavior by increasing ability rather than solely relying on motivation. By making the desired behavior simpler and more accessible, it becomes easier to perform consistently.

* The concept of "shining" is introduced, which involves giving continued attention and effort to the habit to make it feel natural and effortless over time. This helps in solidifying the habit and integrating it seamlessly into one's daily life.

## Question 3: How can you use B = MAP to make making new habits easier?

#### Answer 3:

* The B=MAP framework (Behavior = Motivation + Ability + Prompt) can be applied to facilitate the formation of new habits. Here's how you can use it:

* **Motivation:** Boost your motivation by finding a strong and compelling reason for adopting the habit you want to develop.

* **Ability:** Simplify the habit by breaking it down into small and achievable steps, making it easier to perform consistently.

* **Prompt:** Set up cues or prompts that serve as reminders for you to engage in the habit. This can be accomplished through various methods like setting reminders on your phone or placing notes in strategic locations around your environment.

## Question 4:  Why it is important to "Shine" or Celebrate after each successful completion of habit?

#### Answer 4:

* Celebrating after completing a habit reinforces it in our brains and increases our motivation to repeat the behavior.

* When we celebrate, feel-good chemicals are released in our brains, making us more inclined to continue the habit.

* Celebrating builds our belief in our ability to continue the habit and keeps us motivated.

* Positive emotions generated by celebrating make the habit more enjoyable and integrate it into our daily routine.

* By giving continued effort and attention to the habit, we nurture and grow it over time, making it feel natural and seamlessly integrated into our daily life.

## Question 5: Your takeaways from the video (Minimum 5 points)

#### Answer 5:

* The aggregation of marginal gains: Small improvements, tiny habits, and little choices can lead to significant success over time.

* Creating specific plans and designing the environment for positive habits: Establishing core habits, giving goals a specific time and place, and being intentional about the environment can support successful habit formation.

* The "two-minute rule": Starting any habit in less than two minutes makes it easier to get started and increases the likelihood of follow-through.

* "Liking" as a factor in repeating behaviors: We repeat behaviors because we enjoy the rewards or consequences associated with them, and bringing rewards into the present moment can make habits stick.

* The "Seinfeld strategy": Using a wall calendar to track daily progress on a task and leveraging short-term feedback for long-term behavior change.

## Question 6: Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes.

#### Answer 6:

* James Clear's "Atomic Habits" offers a multifaceted perspective on habit formation.

* **Identity:** Habits are not just about what you do, but also about who you believe you are. Aligning habits with your desired identity helps shape and reinforce your sense of self.

* **Processes:** Clear presents the habit loop framework, emphasizing the importance of cues, cravings, responses, and rewards. Manipulating these components optimizes habit formation. Habit stacking and habit tracking are effective processes.

* **Outcomes:** Focusing on small, incremental changes is key. The 1% rule highlights how small habit improvements compound over time for significant positive outcomes.

* By aligning habits with identity, optimizing processes, and focusing on incremental changes, lasting personal growth and improvement can be achieved.

## Question 7: Write about the book's perspective on how to make a good habit easier.

#### Answer 7:

* In "Atomic Habits," James Clear provides insights on simplifying habit development and maintenance.

* The concept of "habit stacking" involves linking a new habit to an existing one for easier adoption.

* Designing the environment to support good habits is crucial, making desired behaviors easier and undesired behaviors harder.

* Reducing friction by making the first step of a habit as easy as possible is emphasized.

* By implementing these strategies, you can create an environment that promotes positive habits and facilitates goal achievement.

## Question 8: Write about the book's perspective on making a bad habit more difficult?

#### Answer 8:

* In "Atomic Habits," James Clear highlights strategies to break the grip of bad habits.

* Identify and remove triggers or cues that prompt the bad habit.

* Introduce friction or obstacles to make the bad habit more difficult to engage in.

* Change the environment to make bad habits less accessible and replace them with healthier alternatives.

* Increasing the effort required to engage in negative habits disrupts their automaticity and makes them less appealing over time.

## Question 9: Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

#### Answer 9:

To establish a habit of practicing coding exercises regularly:

* Make the cue obvious by setting a specific time and place for coding exercises and creating a designated workspace.

* Make the habit more attractive by finding coding challenges aligned with your interests and incorporating enjoyable elements like gamification or collaboration.

* Make the habit easy by breaking down exercises into smaller tasks, setting realistic goals, and removing distractions or barriers.

* Make the response satisfying by tracking progress, celebrating achievements, and seeking feedback or sharing progress with others.
By following these steps, you can enhance your coding skills as a junior software engineer.

## Question 10: Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

#### Answer 10:

To eliminate the habit of excessive social media use during work hours:

* Make the cue invisible by disabling notifications and keeping your phone in another room.

* Make the process unattractive by associating negative consequences with social media distractions and creating a list of tasks delayed by procrastination.

* Make the response unsatisfying by establishing a "cost" for engaging in social media, such as completing work or reaching a milestone before allowing access.

By implementing these steps, you can reduce the appeal and repetition of the habit, improving focus and productivity as a junior software engineer.
