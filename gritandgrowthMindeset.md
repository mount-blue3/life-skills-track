# Grit and Growth Mindset

## Question1: Paraphrase (summarize) the video in a few lines. Use your own words.

#### Answer 1:

* There is a video that tells the story of a woman who became a teacher at a young age. During her teaching career, she noticed that students with lower IQ scores were performing exceptionally well, while some students with high IQ scores were not meeting expectations.

* After carefully analyzing various factors, she realized that performance is not solely determined by IQ. Other factors play a significant role.

* To gain a deeper understanding of the brain's learning process, she decided to study psychology and focused on different age groups to explore how learning occurs.

* Through her research, she came to the conclusion that our learning process is not fixed; it can be altered through our efforts.

* She discovered that having grit is essential when it comes to learning. Learning is not a short-term endeavor; it requires patience and perseverance.

## Question 2: What are your key takeaways from the video to take action on?

#### Answer 2:

* Grit is highlighted as a crucial factor in achieving success in learning.

* Changing our mindset can significantly impact our learning abilities.

* Learning requires time and patience; rushing the process is not effective.

## Question 3: Paraphrase (summarize) the video in a few lines in your own words.

#### Answer 3:

* Carol Dweck's extensive research spans decades, focusing on why some equally talented individuals succeed while others do not. Her findings emphasize the crucial role of mindset in this process.

* Two mindsets exist: the fixed mindset and the growth mindset.

* The fixed mindset believes that abilities are innate and cannot be changed.

* The growth mindset believes that abilities can be developed and improved over time.

* Lebron James highlights the growth mindset's emphasis on personal growth and improvement.

* The video clearly distinguishes between the growth mindset and the fixed mindset.

* The growth mindset embraces challenges, sees mistakes as opportunities for feedback, and prioritizes learning over immediate results.

* In contrast, the fixed mindset prioritizes outcomes, avoids challenges, seeks comfort, and disregards feedback.

## Question 4: What are your key takeaways from the video to take action on?

#### Answer 4:

* By exerting effort, we have the capacity to transform our learning abilities and experience personal growth.

* It is essential to prioritize the learning process rather than being solely fixated on achieving specific goals or outcomes.

* Challenges serve as valuable opportunities to acquire new knowledge and skills.

* Feedback plays a crucial role in fostering a growth mindset, encouraging continuous improvement and development.

## Question 5: What is the Internal Locus of Control? What is the key point in the video?

#### Answer 5:

Three types of people: god-gifted mindset, hard-working mindset, and lazy mindset.

* God-gifted individuals focus on easy tasks, get demotivated by failure, and have an external locus of control.

* Hard-working individuals enjoy challenges, exert effort, and have an internal locus of control.

* Locus of control determines motivation.

## Question 6: Paraphrase (summarize) the video in a few lines in your own words.

#### Answer 6:

* Based on my observations, this video explores the concepts of acceptance mindset and unacceptance mindset. If you believe that you have no control over the circumstances in your life, it hinders any possibility of change.

* However, if you analyze and recognize that you can improve and do better, taking action and making efforts will lead to growth.

* A fixed mindset is when you perceive yourself as unchangeable, resulting in a lack of motivation to make any changes.

## Question 7: What are your key takeaways from the video to take action on?

#### Answer 7:

* Believing in a lifetime of continuous improvement and growth.

* Challenging limiting assumptions to unlock our full potential.

* Creating a personal life curriculum that aligns with our passions and dreams.

* Valuing and learning from struggles and failures, nurturing our passion for ongoing growth.

## Question 8: What are one or more points that you want to take action on from the manual?

#### Answer 8:

* I hold complete responsibility for my learning journey.

* I will assume full ownership of the projects assigned to me, ensuring their execution, delivery, and functionality.

* I will approach new challenges and interactions with enthusiasm, maintaining a positive attitude.

* I will not leave my code incomplete until I satisfy the following checklist:

   1.Ensure it functions correctly.

   2.Make it readable and understandable.
   
   3.Optimize it for efficiency.