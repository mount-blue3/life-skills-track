#  Good Practices for Software Development

## Question 1

### What is your one major takeaway from each one of the 6 sections. So 6 points in total.

## Answer 1

* **Gathering requirements:** Ask questions to understand what is needed. It's hard to get everyone together again, so take the chance to get clarity while I can. Develop the habit of noting down key points.

* **Always over-communicate Some scenarios:** Don't miss calls. If I can't talk, answer and say I will call back soon. It's better than ignoring the call.

* **Stuck? Ask questions:** If I am stuck, I have to explain the problem and what I have tried so far to fix it.

* **Get to know your teammates:** Take time to learn about my company, product, and team. It helps with better communication.

* **Be aware and mindful of other team members:** Remembering that my teammates also have their own work. So I have to choose the right way to communicate based on the situation.

* **Doing things with 100% involvement:** Exercise to keep my energy levels up and be fully engaged in my work.

## Question 2

### Which area do you think you need to improve on? What are your ideas to make progress in that area?

## Answer 2

1.Focus management:

* Start practicing meditation to enhance focus.
* Establish a daily routine that promotes productivity.

2.Emotional and physical health:

* Prioritize mindfulness practices to balance emotions.
* Listen to mind-relaxing soothing music.
* Take care of physical health through exercise, proper nutrition, and rest.

3.Doing things with 100% involvement:

* Shift focus from outcomes to enjoying the process.
* Cultivate a growth mindset to foster engagement.
* Create a dedicated and suitable workspace to minimize distractions.

4.Asking questions when stuck:

* Attempt to solve problems independently with a set time limit.
* If unresolved, assess the importance and potential impact before seeking help.
* Seek assistance from colleagues or mentors based on the problem's significance and future implications.